import string
import random

# VARIABLES

# Main characters used to create the password.
pwdCharacters = string.ascii_lowercase


# QUESTIONS FOR THE USER BEFORE GENERATE THE PASSWORDS

print('The default characters are lowercase!')

uppercase = input('Do you want to use uppercase ? (y/n) \n')


while uppercase is not "y" and uppercase is not "n":
    print('The response only can be "y" or "n"')
    uppercase = input('Do you want to use uppercase ? (y/n)\n')


digits = input('Do you want to use digits ? (y/n)\n')
while digits != 'y' and digits != 'n':
    print('The response only can be "y" or "n"')
    digits = input('Do you want to use digits ? (y/n)\n')

punctuation = input('Do you want to use punctuation ? (y/n)\n')
while punctuation != 'y' and punctuation != 'n':
    print('The response only can be "y" or "n"')
    punctuation = input('Do you want to use punctuation ? (y/n)\n')


numberPasswords = int(input('How many passwords do you want to generate ?\n'))
print('{} passwords will be generated\n'.format(numberPasswords))

numberCharacters = int(input('How many characters does the password have to have ?\n'))
print('The password have {} characters \n'.format(numberCharacters))


# CHECK THE RESPONSE OF THE QUESTIONS AND ADD CHARACTERS (OR NOT).


# If the user set to 'y' the variable uppercase, we add the uppercase letters to the variable 'pwdCharacters'
if uppercase is 'y':
    pwdCharacters = pwdCharacters + string.ascii_uppercase

# If the user set to 'y' the variable digits, we add the digits letters to the variable 'pwdCharacters'
if digits is 'y':
    pwdCharacters = pwdCharacters + string.digits

# If the user set to 'y' the variable punctuation, we add the punctuation letters to the variable 'pwdCharacters'
if punctuation is 'y':
    pwdCharacters = pwdCharacters + string.punctuation


# FUNCTIONS


def pwd_generate(numbercharacters, numberpasswords, pwdcharacters):
    '''
    This function will generate passwords with the conditions that users select.
    :param numbercharacters: Number of characters the password will have.
    :param numberpasswords: Number of passwords to be generate.
    :param pwdcharacters: The characters that the password will have.
    :return: Array with the generated passwords.
    '''

    passwordarray = []

    while numberpasswords > 0:
        generatedpasswords = ''.join(random.choice(pwdcharacters) for _ in range(numbercharacters))
        numberpasswords = numberpasswords - 1
        passwordarray.append(generatedpasswords)

    return passwordarray


# CALL THE FUNCTION AND SAVE THE RESULT ON TO VARIABLE


passwords = pwd_generate(numberCharacters, numberPasswords, pwdCharacters)

# PRINT THE RESULT OF THE PASSWORDS

print('Passwords generated:')
for password in passwords:
    print(password)
